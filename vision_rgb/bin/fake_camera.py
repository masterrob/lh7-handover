#! /usr/bin/env python3

import rospy
from std_msgs.msg import String
from sensor_msgs.msg import Image
from webots_ros.srv import set_int
from vision_utils.img import numpy_to_image, bgr_to_rgb, rgb_to_bgr
from skimage.io import imread
import numpy as np

model_name = "fakecamera"
CAMERA_ENABLED = False

rospy.init_node('fake_camera')
model_publisher = rospy.Publisher("/model_name", String, queue_size=1)
image_publisher = rospy.Publisher(model_name + "/camera/image", Image, queue_size=20)


def camera_enable(req):
    response = set_int()
    response.success = True
    print("Camera enabled")
    CAMERA_ENABLED = True
    return True


rospy.Service(model_name + "/camera/enable", set_int, camera_enable)

# image = imread("/home/glhr/catkin_ws/src/lh7-nlp/vision_rgb/test/1589237135 test-orig.png")
image = imread("/home/glhr/catkin_ws/src/lh7-nlp/vision_rgb/test/1589273093 test-orig.png")
image = rgb_to_bgr(image)

while not rospy.is_shutdown():
    model_publisher.publish(model_name)
    # print("published model name")
    image_publisher.publish(numpy_to_image(image))
    rospy.sleep(1)

print("shutdown")
