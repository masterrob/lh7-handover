#! /usr/bin/env python

import roslib
roslib.load_manifest('darknet_ros')
import rospy
import actionlib

from darknet_ros_msgs.msg import CheckForObjectsAction, CheckForObjectsGoal
from sensor_msgs.msg import Image

if __name__ == '__main__':
    rospy.init_node('check_object_client')
    
    client = actionlib.SimpleActionClient('/darknet_ros/check_for_objects', CheckForObjectsAction)
    print("Waiting for server")
    client.wait_for_server()
    
    goal = CheckForObjectsGoal()
    goal.id = 1
    print("Waiting for image")
    goal.image = rospy.wait_for_message("/wrist_camera/camera/color/image_raw", Image, timeout=1)
    
    client.send_goal(goal)
    print("Waiting for action result")
    client.wait_for_result(rospy.Duration.from_sec(5.0))
    print(client.get_result())