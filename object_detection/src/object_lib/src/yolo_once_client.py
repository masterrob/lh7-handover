#!/usr/bin/env python

import sys
import rospy
from sensor_msgs.msg import Image
from object_lib.srv import YoloOnce


def yolo_once_client(img):
    rospy.wait_for_service('yolo_once')
    try:
        yolo_once = rospy.ServiceProxy('yolo_once', YoloOnce)
        resp1 = yolo_once(img)
        return resp1.bounding_boxes
    except rospy.ServiceException as e:
        print("Service call failed: %s"%e)


if __name__ == "__main__":
    rospy.init_node("yolo_once_client")
    # rospy.Subscriber("/ptu_camera/camera/color/image_raw", Image, img_callback)
    print("Requesting ...")
    img = rospy.wait_for_message("/ptu_camera/camera/color/image_raw", Image, timeout=None)
    print("Reponse: ", yolo_once_client(img))

