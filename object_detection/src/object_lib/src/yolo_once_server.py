#!/usr/bin/env python

import rospy

from sensor_msgs.msg import Image
from darknet_ros_msgs.msg import BoundingBoxes

from object_lib.srv import YoloOnce


BBOX = BoundingBoxes()

def bb_callback(msg):
    global BBOX
    BBOX = msg

def handle_yolo_once(req):
    print("Looking once")
    pub = rospy.Publisher('/camera/rgb/image_raw', Image, queue_size=10)
    pub.publish(req.input)

    return BBOX

def yolo_once_server():

    rospy.init_node('yolo_once_server')
    s = rospy.Service('yolo_once', YoloOnce, handle_yolo_once)
    rospy.Subscriber('/darknet_ros/bounding_boxes', BoundingBoxes, bb_callback)

    print("Ready to handle yolo requests.")
    rospy.spin()

if __name__ == "__main__":
    yolo_once_server()

