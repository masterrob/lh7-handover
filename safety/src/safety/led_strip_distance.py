#! /usr/bin/env python

import numpy as np
import rospy
from std_msgs.msg import String
from geometry_msgs.msg import Pose
from pose_utils.utils import better_distance_between_points


def centroids_callback(msg):

    distance = better_distance_between_points([0,0],[msg.position.x,msg.position.y])

    if distance < 1.5:
        color_pub.publish("u")
        print("red")
    else:
        color_pub.publish("y")
        print("yellow")


rospy.init_node("led_distance")
#Need to change topic
centroid_sub = rospy.Subscriber('/kalman_test', Pose, centroids_callback)
color_pub = rospy.Publisher("/led_strip",String,queue_size=1)
print("Led distance node running")
rospy.spin()
