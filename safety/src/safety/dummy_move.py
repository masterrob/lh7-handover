#! /usr/bin/env python

from robot_moveit_commander import robot_class as robot
import rospy
from geometry_msgs.msg import PoseStamped, Pose
from actionlib_msgs.msg import GoalStatusArray
from std_msgs.msg import Float32MultiArray
from std_srvs.srv import Empty, EmptyResponse
import yaml
import os

import pathlib
script_path = pathlib.Path(__file__).parent.absolute()

rospy.init_node('dummy_move')

# Reset robot to an initial state (home and ready, gripper oppened)
robot_client = robot.Robot()
# robot_client.rs_gripper.startupGripper()

# Moving robots to starting position
# robot_client.setNamedTarget("home", "left", move=True)
robot_client.setNamedTarget("ready", "right", move=True)
# robot_client.rs_gripper.genCommand("left_open")

# Puts the node into a loop doing only callbacks
print("Dummy move ready")

group = robot_client.right_arm
current_pose = group.get_current_pose().pose
print(current_pose)

pose = current_pose

# positions = [
#                 [0.1,   0.45],
#                 [0,     0.45],
#                 [-0.1,  0.45],
#                 [-0.1,  0.35],
#                 [-0.1,  0.25],
#                 [0,     0.25],
#                 [0.1,   0.25],
#                 [0.1,   0.35]
#             ]

positions = [
                [0.4,   0.45],
                [0.6,   0.45]
            ]

i = 0



while not rospy.is_shutdown():
    x,y = positions[i]
    pose.position.x = x
    pose.position.y = y
    plan, error = robot_client.setTarget(pose, "right", move=True, return_plan=True)
    print(plan)
    print(i)
    file_path = script_path.parent / 'saved_trajectories' / 'plan-{}.yaml'.format(i)
    with open(str(file_path), 'w') as file_save:
        yaml.dump(plan, file_save, default_flow_style=True)
    if i<len(positions)-1: i += 1
    else: i=0

        #rospy.sleep(2)
# pose.position.y = position[1]
# pose.position.z = position[2]
#
# pose.orientation.x = rotation[0]
# pose.orientation.y = rotation[1]
# pose.orientation.z = rotation[2]
# pose.orientation.w = rotation[3]
