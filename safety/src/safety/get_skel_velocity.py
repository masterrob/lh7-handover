#! /usr/bin/env python3

import rospy
from sensor_msgs.msg import JointState
from vision_utils.logger import get_logger

from safety.msg import RobotArm, Distance
from human_pose_ros.msg import Skeleton, PoseEstimation
from pose_utils.utils import *
from rospy_message_converter import message_converter
import time
import vg
import numpy as np

logger = get_logger()

WINDOW_LENGTH = 10

rospy.init_node("skel_velocity_node_{}".format(WINDOW_LENGTH))
skel_velocity_pub = rospy.Publisher('skel_velocity_{}'.format(WINDOW_LENGTH), Skeleton, queue_size=1)

old_skel, old_skel_times, old_skel_time = dict(), dict(), dict()
old_skel["filtered"] = dict()
old_skel["raw"] = dict()
old_skel_times["raw"], old_skel_times["filtered"] = dict(),dict()
old_skel_time["raw"], old_skel_time["filtered"] = [0]*WINDOW_LENGTH,[0]*WINDOW_LENGTH

history = dict()

def add_to_history(entry, key, val):
    entry[key] = entry.get(key,[0]*WINDOW_LENGTH)

    entry[key].append(val)

    if len(entry[key]) > WINDOW_LENGTH:
        entry[key] = entry[key][-WINDOW_LENGTH:]

    return entry[key]



def velocity_from_skeleton(msg, filtered=False):
    global old_skel
    if filtered:
        select_skel = "filtered"
    else:
        select_skel = "raw"
    person_id = msg.tracked_person_id
    try:
        skeleton = msg.skeletons[person_id]
    except Exception as e:
        # logger.warning("No skeleton received")
        return Skeleton()

    skel_dict = message_converter.convert_ros_message_to_dictionary(skeleton)
    skel_velocities_dict = dict()

    dt = time.time()

    for joint,coord in skel_dict.items():
        if not isinstance(coord,list):
            continue
        curr_time = time.time()
        dt = curr_time - max(old_skel_times[select_skel].get(joint,[0]*WINDOW_LENGTH)[-WINDOW_LENGTH], old_skel_time[select_skel][-WINDOW_LENGTH])
        if len(skel_dict[joint]):
            if old_skel[select_skel].get(joint,0) and len(old_skel[select_skel][joint]):
                joint_filtered = skel_dict[joint]
                try:
                    dx = joint_filtered[0] - old_skel[select_skel][joint][-WINDOW_LENGTH][0]
                    dy = joint_filtered[1] - old_skel[select_skel][joint][-WINDOW_LENGTH][1]
                    dz = joint_filtered[2] - old_skel[select_skel][joint][-WINDOW_LENGTH][2]

                    v = np.array([dx/dt, dy/dt, dz/dt])
                    skel_velocities_dict[joint] = [vg.magnitude(v)]
                    # print(skel_velocities_dict[joint])
                except TypeError:
                    pass

            old_skel[select_skel][joint] = add_to_history(old_skel[select_skel],joint,skel_dict[joint])
            old_skel_times[select_skel][joint] = add_to_history(old_skel_times[select_skel],joint,curr_time)

    old_skel_time[select_skel] = add_to_history(old_skel_time, select_skel, time.time())
    return message_converter.convert_dictionary_to_ros_message("human_pose_ros/Skeleton", skel_velocities_dict)


def pose_callback(msg):
    global distances
    # msg_vel_filtered = velocity_from_skeleton(msg, filtered=True)
    msg_vel = velocity_from_skeleton(msg, filtered=False)
    # skel_velocity_filtered_pub.publish(msg_vel)
    skel_velocity_pub.publish(msg_vel)

pose_filtered_sub = rospy.Subscriber('openpifpaf_pose_transformed_kalman_world', PoseEstimation, pose_callback)

rospy.spin()
