#! /usr/bin/env python

import rospy

from vision_utils.logger import get_logger
from safety.msg import Vector
from geometry_msgs.msg import TwistStamped, Vector3
from controller_manager_msgs.srv import SwitchController

logger = get_logger()


def vector_callback(msg):
    twist_msg = TwistStamped()
    if len(msg.right_wrist_to_left_ee) > 1:
        twist_msg.header.frame_id = ""
        twist_msg.header.stamp = rospy.Time.now()

        linear_vector = Vector3()
        linear_vector.x = msg.right_wrist_to_left_ee[0]*10
        linear_vector.y = msg.right_wrist_to_left_ee[1]*10
        linear_vector.z = msg.right_wrist_to_left_ee[2]*10

        angular_vector = Vector3()
        angular_vector.x = 0
        angular_vector.y = 0
        angular_vector.z = 0

        twist_msg.twist.linear = linear_vector
        twist_msg.twist.angular = angular_vector
        print(twist_msg)
        servo_pub.publish(twist_msg)

rospy.init_node("follow_node")
servo_pub = rospy.Publisher('left_servo_server/delta_twist_cmds', TwistStamped, queue_size=1)
vector_sub = rospy.Subscriber('vectors_human_robot', Vector, vector_callback)

rospy.wait_for_service('/controller_manager/switch_controller')
client = rospy.ServiceProxy('/controller_manager/switch_controller', SwitchController)

resp1 = client(["left_joint_group_position_controller", "right_joint_group_position_controller"], ["left_arm_controller", "right_arm_controller"],1,False,0.0)
print(resp1)


rospy.spin()
