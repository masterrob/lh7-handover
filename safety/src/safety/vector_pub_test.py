#! /usr/bin/env python

import rospy

from safety.msg import Vector
from geometry_msgs.msg import TwistStamped, Vector3
from controller_manager_msgs.srv import SwitchController



rospy.init_node("vector_pub_node")
servo_pub = rospy.Publisher('left_servo_server/delta_twist_cmds', TwistStamped, queue_size=1)

rospy.wait_for_service('/controller_manager/switch_controller')
client = rospy.ServiceProxy('/controller_manager/switch_controller', SwitchController)
resp1 = client(["left_joint_group_position_controller", "right_joint_group_position_controller"], ["left_arm_controller", "right_arm_controller"],1,False,0.0)
print(resp1)

rate = rospy.Rate(100)
while not rospy.is_shutdown():
    twist_msg = TwistStamped()
    twist_msg.header.frame_id = ""
    twist_msg.header.stamp = rospy.Time.now()

    linear_vector = Vector3()
    linear_vector.x = -0.1
    linear_vector.y = 0.0
    linear_vector.z = 0.0

    angular_vector = Vector3()
    angular_vector.x = 0
    angular_vector.y = 0
    angular_vector.z = 0

    twist_msg.twist.linear = linear_vector
    twist_msg.twist.angular = angular_vector
    print(twist_msg)
    servo_pub.publish(twist_msg)
    rate.sleep()
