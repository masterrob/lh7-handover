#!/usr/bin/env python3

import rospy
from std_msgs.msg import Float32MultiArray
from visualization_msgs.msg import Marker


colours = {
    'red': (1.0,0.0,0.0),
    'yellow': (1.0,0.8,0),
    'green': (0.1, 1, 0)
}

joints = ["base", "shoulder", "forearm", "wrist_1", "wrist_2", "wrist_3", "ee"]
# joints = ["ee"]

topic_hash = abs(int(str(hash("safety"))[:9]))

rospy.init_node('zone_visualisation_node')

zone = Marker()

zone.header.stamp = rospy.get_rostime()
zone.ns = 'safety_zone_red'
zone.type = zone.SPHERE
zone.action = zone.ADD

zone.pose.position.x = 0
zone.pose.position.y = 0
zone.pose.position.z = 0
zone.pose.orientation.w = 1.0
zone.frame_locked = True
zone.lifetime = rospy.Duration(1)


def safety_callback(msg):
    for count,joint in enumerate(joints):

        zone.header.frame_id = "ur5_right_{}_link".format(joint)

        for action in [zone.ADD]:
            zone.action = action
            zone.ns = 'safety_zone_red'
            zone.id = count
            zone.color.a = 0.4
            zone.scale.x, zone.scale.y, zone.scale.z = msg.data[0]*2, msg.data[0]*2, msg.data[0]*2
            zone.color.r, zone.color.g, zone.color.b = colours['red']
            if joint == "ee": safezones_pub.publish(zone)

            zone.ns = 'safety_zone_yellow'
            zone.color.a = 0.3
            zone.scale.x, zone.scale.y, zone.scale.z = msg.data[1]*2, msg.data[1]*2, msg.data[1]*2
            zone.color.r, zone.color.g, zone.color.b = colours['yellow']
            if joint == "ee": safezones_pub.publish(zone)


rospy.Subscriber('/safety_commander', Float32MultiArray, safety_callback)
safezones_pub = rospy.Publisher('safe_zones', Marker, queue_size=100)

rospy.spin()
