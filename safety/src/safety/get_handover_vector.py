#! /usr/bin/env python

import rospy
from sensor_msgs.msg import JointState
from vision_utils.logger import get_logger

import tf
from tf.transformations import quaternion_from_euler, quaternion_multiply
from safety.msg import RobotArm, Vector
from human_pose_ros.msg import Skeleton, PoseEstimation
from pose_utils.utils import vector_coords_from_2_points
from rospy_message_converter import message_converter

logger = get_logger()

vectors = dict()
robot_left_arm = dict()
robot_right_arm = dict()

def update_vectors(vectors, vector_name, skeleton_joint, robot_arm, robot_joint='ee'):
    if skeleton_joint and robot_arm.get(robot_joint,0):
        vectors[vector_name] = vector_coords_from_2_points(robot_arm[robot_joint], skeleton_joint[0:3])
    return vectors

def pose_callback(msg):
    global vectors
    person_id = msg.tracked_person_id

    if len(msg.skeletons):
        skeleton = msg.skeletons[person_id]

        for robot_link in ['ee']:
            # wrists
            vectors = update_vectors(vectors, 'left_wrist_to_right_{}'.format(robot_link), skeleton.left_wrist, robot_right_arm, robot_joint=robot_link)
            vectors = update_vectors(vectors, 'right_wrist_to_right_{}'.format(robot_link), skeleton.right_wrist, robot_right_arm, robot_joint=robot_link)
            vectors = update_vectors(vectors, 'left_wrist_to_left_{}'.format(robot_link), skeleton.left_wrist, robot_left_arm, robot_joint=robot_link)
            vectors = update_vectors(vectors, 'right_wrist_to_left_{}'.format(robot_link), skeleton.right_wrist, robot_left_arm, robot_joint=robot_link)
            
        msg_vectors = message_converter.convert_dictionary_to_ros_message("safety/Vector",vectors)
        vectors_pub.publish(msg_vectors)


rospy.init_node("vector_node")
left_arm_pub = rospy.Publisher('positions_robot_left_arm', RobotArm, queue_size=1)
right_arm_pub = rospy.Publisher('positions_robot_right_arm', RobotArm, queue_size=1)

vectors_pub = rospy.Publisher('vectors_human_robot', Vector, queue_size=1)
pose_sub = rospy.Subscriber('openpifpaf_pose_transformed_pose_world', PoseEstimation, pose_callback)

tf_listener = tf.TransformListener()

def update_robot_arm(arm='left', link='ee', tf=''):
    global robot_left_arm
    tf = '/ur5_' + arm + '_' + link + '_link' if not len(tf) else tf
    (trans, rot) = tf_listener.lookupTransform('/world', tf, rospy.Time())
    if arm=='left':
        robot_left_arm[link] = trans
        msg = message_converter.convert_dictionary_to_ros_message("safety/RobotArm",robot_left_arm)
        left_arm_pub.publish(msg)
    elif arm == 'right':
        robot_right_arm[link] = trans
        msg = message_converter.convert_dictionary_to_ros_message("safety/RobotArm",robot_right_arm)
        right_arm_pub.publish(msg)

while not rospy.is_shutdown():
    try:
        tf_listener.waitForTransform('/world', '/ur5_left_ee_link', rospy.Time(), rospy.Duration(0.1))

        update_robot_arm(arm='left', link='ee', tf='')
        update_robot_arm(arm='right', link='tool', tf='/right_ee_link')
        update_robot_arm(arm='right', link='ee')
        update_robot_arm(arm='right', link='shoulder')
        update_robot_arm(arm='right', link='forearm')
        update_robot_arm(arm='right', link='wrist_1')
    except Exception as e:
        pass
