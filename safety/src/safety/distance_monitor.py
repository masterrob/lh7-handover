#! /usr/bin/env python3
import rospy
from geometry_msgs.msg import PoseStamped, Pose
from safety.msg import RobotArm, Distance, SafetyInfo
from actionlib_msgs.msg import GoalStatusArray
from std_msgs.msg import Float32MultiArray
from std_msgs.msg import Float32, Bool
from std_srvs.srv import Empty, EmptyResponse, Trigger
from ur_msgs.srv import SetSpeedSliderFraction
from rospy_message_converter import message_converter
from vision_utils.logger import get_logger
from human_pose_ros.msg import Skeleton, PoseEstimation
import numpy as np

logger = get_logger()
min_distance = 0
max_speed = 0

PLAY = False
current_speed = 0

rospy.init_node("distance_monitor")

velocity_joints = ["right_wrist", "left_wrist", "right_elbow", "left_elbow", "right_shoulder", "left_shoulder", "nose"]

boundaries = dict()
boundaries['red'] = 0.5
boundaries['orange'] = 2
boundaries['green'] = 10000

zone = 'red'

def update_zone():
    global zone
    prev_zone = zone
    if min_distance < boundaries['red']:
        zone = 'red'
    elif min_distance < boundaries['orange']:
        zone = 'orange'
    else:
        zone = 'green'
    if zone != prev_zone:
        logger.info(f"Entered zone: {zone}")

def boundaries_cb(msg):
    global boundaries
    boundaries['red'] = msg.data[0]
    boundaries['orange'] = msg.data[1]
    logger.info(f"Updated boundaries: {boundaries}")
    update_zone()
    update_safety_info()

def update_safety_info():
    info = SafetyInfo()
    info.min_distance = min_distance
    info.max_speed = max_speed
    info.zone = zone
    safety_pub.publish(info)

def distance_cb(msg):
    global min_distance
    distances = message_converter.convert_ros_message_to_dictionary(msg)
    # logger.debug(distances)
    distances = {k:v for k,v in distances.items() if v>0 and "_".join(k.split("_")[0:2]) in velocity_joints}
    if len(distances):
        min_distance = np.min(list(distances.values()))
    # logger.debug("Min robot-human distance: {}".format(min_distance))
    update_zone()
    update_safety_info()


def vel_cb(msg):
    global max_speed
    speeds = message_converter.convert_ros_message_to_dictionary(msg)

    speeds = {k:v for k,v in speeds.items() if isinstance(v,list) and len(v) and v[0]>0 and k in velocity_joints}
    if len(speeds):
        max_speed = np.max(list(speeds.values()))
    # logger.debug(max_speed)
    update_safety_info()

NOPERSON = False

def noperson_cb(msg):
    global NOPERSON
    NOPERSON = msg.data

distances_sub = rospy.Subscriber('distance_human_robot', Distance, distance_cb)
commander_sub = rospy.Subscriber('safety_commander',Float32MultiArray, boundaries_cb)
speed_pub = rospy.Publisher('robot_speed', Float32, queue_size=1)
noperson_sub = rospy.Subscriber('noperson', Bool, noperson_cb)
vel_sub = rospy.Subscriber('skel_velocity_10', Skeleton, vel_cb)
safety_pub = rospy.Publisher('safety', SafetyInfo, queue_size=1)

right_pause_serv = rospy.ServiceProxy('/right_arm/ur_hardware_interface/dashboard/pause', Trigger)
right_play_serv = rospy.ServiceProxy('/right_arm/ur_hardware_interface/dashboard/play', Trigger)
right_speed_serv = rospy.ServiceProxy('/right_arm/ur_hardware_interface/set_speed_slider', SetSpeedSliderFraction)

MAX_ROBOT_SPEED = 0.7
MIN_ROBOT_SPEED = 0.02

def robot_speed_from_distance(distance, lower_bound, upper_bound):
    max_speed = MAX_ROBOT_SPEED
    min_speed = MIN_ROBOT_SPEED
    slope = (max_speed - min_speed) / (upper_bound - lower_bound)
    intercept = min_speed - slope * lower_bound

    speed = slope * distance + intercept

    return speed



def resume_program(speed=None, increase=False):
    global current_speed

    speed = 0.1 if speed is None else speed

    speed = min(current_speed+0.001,speed)
    # if abs(speed-current_speed) < 0.0001:
    #     speed = current_speed
    speed = max(min(MAX_ROBOT_SPEED,speed),MIN_ROBOT_SPEED) # cap the speed
    try:
        right_speed_serv.call(speed)
    except rospy.service.ServiceException as e:
        # logger.error("Couldn't connect to UR service")
        # rospy.sleep(1)
        pass
    current_speed = speed
    # logger.warning("Speed set to {} (min distance {}, max speed {})".format(speed_scale, min_distance, max_speed))

def stop_program():
    global current_speed
    current_speed = 0.02
    try:
        right_speed_serv.call(0.02)
    except rospy.service.ServiceException as e:
        # logger.error("Couldn't connect to UR service")
        # rospy.sleep(1)
        pass

ignore_no_person = True

while not rospy.is_shutdown():
    # logger.debug(current_speed)
    speed_pub.publish(current_speed)

    if zone == "red" and (not NOPERSON or ignore_no_person):
        stop_program()
        logger.warning("--> Program paused (min distance {}, max speed {})".format(min_distance, max_speed))
    elif zone == "orange" and max_speed > 1 and (not NOPERSON or ignore_no_person):
        resume_program(speed=current_speed-0.05)
    elif zone == "orange" and (not NOPERSON or ignore_no_person):
        # val = min(min_distance, (max(0,1-max_speed))
        speed_scale = robot_speed_from_distance(min_distance, lower_bound=boundaries['red'], upper_bound=boundaries["orange"])
        resume_program(speed=speed_scale)

    elif (NOPERSON or ignore_no_person) or zone == "green":
        # logger.warning("NO PERSON (according to RUne)")
        resume_program(speed=current_speed + 0.1)
