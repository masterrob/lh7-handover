#! /usr/bin/env python

import rospy
from sensor_msgs.msg import JointState
from vision_utils.logger import get_logger

import tf
from tf.transformations import quaternion_from_euler, quaternion_multiply
from safety.msg import RobotArm, Distance
from human_pose_ros.msg import Skeleton, PoseEstimation
from pose_utils.utils import distance_between_points
from rospy_message_converter import message_converter

logger = get_logger()

pan_pos = 0
tilt_pos = 0

distances = dict()
robot_left_arm = dict()
robot_right_arm = dict()

def js_callback(msg):
    if "ptu_panner" in msg.name:
        pan_pos, tilt_pos = joint_msg.position

def update_distances(distances, distance_name, skeleton_joint, robot_arm, robot_joint='ee'):
    if skeleton_joint and robot_arm.get(robot_joint,0):
        distances[distance_name] = distance_between_points(robot_arm[robot_joint], skeleton_joint[0:3])
        # logger.info(distance_name)
        # logger.debug(distances[distance_name])
    # else:
    #     print(skeleton_joint)
    return distances

def pose_callback(msg):
    global distances
    person_id = msg.tracked_person_id

    if len(msg.skeletons):
        skeleton = msg.skeletons[person_id]

        for robot_link in ['ee','forearm','shoulder', 'wrist_1']:
            # centroid
            distances = update_distances(distances, 'centroid_to_right_{}'.format(robot_link), skeleton.centroid, robot_right_arm, robot_joint=robot_link)
            # shoulders
            distances = update_distances(distances, 'left_shoulder_to_right_{}'.format(robot_link), skeleton.left_shoulder, robot_right_arm, robot_joint=robot_link)
            distances = update_distances(distances, 'right_shoulder_to_right_{}'.format(robot_link), skeleton.right_shoulder, robot_right_arm, robot_joint=robot_link)
            # wrists
            distances = update_distances(distances, 'left_wrist_to_right_{}'.format(robot_link), skeleton.left_wrist, robot_right_arm, robot_joint=robot_link)
            distances = update_distances(distances, 'right_wrist_to_right_{}'.format(robot_link), skeleton.right_wrist, robot_right_arm, robot_joint=robot_link)
            # elbows
            distances = update_distances(distances, 'left_elbow_to_right_{}'.format(robot_link), skeleton.left_elbow, robot_right_arm, robot_joint=robot_link)
            distances = update_distances(distances, 'right_elbow_to_right_{}'.format(robot_link), skeleton.right_elbow, robot_right_arm, robot_joint=robot_link)
            # nose
            distances = update_distances(distances, 'nose_to_right_{}'.format(robot_link), skeleton.nose, robot_right_arm, robot_joint=robot_link)
            # ears
            distances = update_distances(distances, 'left_ear_to_right_{}'.format(robot_link), skeleton.left_ear, robot_right_arm, robot_joint=robot_link)
            distances = update_distances(distances, 'right_ear_to_right_{}'.format(robot_link), skeleton.right_ear, robot_right_arm, robot_joint=robot_link)
            # eyes
            distances = update_distances(distances, 'left_eye_to_right_{}'.format(robot_link), skeleton.left_eye, robot_right_arm, robot_joint=robot_link)
            distances = update_distances(distances, 'right_eye_to_right_{}'.format(robot_link), skeleton.right_eye, robot_right_arm, robot_joint=robot_link)

        msg_distances = message_converter.convert_dictionary_to_ros_message("safety/Distance",distances)
        distances_pub.publish(msg_distances)


rospy.init_node("distance_node")
left_arm_pub = rospy.Publisher('positions_robot_left_arm', RobotArm, queue_size=1)
right_arm_pub = rospy.Publisher('positions_robot_right_arm', RobotArm, queue_size=1)

distances_pub = rospy.Publisher('distance_human_robot', Distance, queue_size=1)
pose_sub = rospy.Subscriber('openpifpaf_pose_transformed_limbs_world', PoseEstimation, pose_callback)

tf_listener = tf.TransformListener()

def update_robot_arm(arm='left', link='ee', tf=''):
    global robot_left_arm
    tf = '/ur5_' + arm + '_' + link + '_link' if not len(tf) else tf
    (trans, rot) = tf_listener.lookupTransform('/world', tf, rospy.Time())
    if arm=='left':
        robot_left_arm[link] = trans
        msg = message_converter.convert_dictionary_to_ros_message("safety/RobotArm",robot_left_arm)
        left_arm_pub.publish(msg)
    elif arm == 'right':
        robot_right_arm[link] = trans
        msg = message_converter.convert_dictionary_to_ros_message("safety/RobotArm",robot_right_arm)
        right_arm_pub.publish(msg)

while not rospy.is_shutdown():
    try:
        tf_listener.waitForTransform('/world', '/ur5_left_ee_link', rospy.Time(), rospy.Duration(0.1))

        update_robot_arm(arm='left', link='ee', tf='')
        update_robot_arm(arm='right', link='tool', tf='/right_ee_link')
        update_robot_arm(arm='right', link='ee')
        update_robot_arm(arm='right', link='shoulder')
        update_robot_arm(arm='right', link='forearm')
        update_robot_arm(arm='right', link='wrist_1')
    except Exception as e:
        pass
