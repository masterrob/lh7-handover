This file explains how system functionality can be partially run/tested without any connection to the LH7+ platform.

### Simulation without any connection to LH7+

To launch the robot model and fake controller:
```shell
roslaunch dummy main.launch mode:=norobot
```

