## Robot Setup

### Prepare the robots

For using the *ur_robot_driver* with a real robot you need to install the `externalcontrol-1.0.urcap` which can be found inside the `ur_robot_driver/resources` folder.

**Note**: For installing this URCap a minimal PolyScope version of 3.7 or 5.1 (in case of e-Series) is necessary.

For installing the necessary URCap and creating a program, please see the individual tutorials on how to [setup a CB3 robot](https://github.com/UniversalRobots/Universal_Robots_ROS_Driver/blob/master/ur_robot_driver/doc/install_urcap_cb3.md) or how to [setup an e-Series robot](https://github.com/UniversalRobots/Universal_Robots_ROS_Driver/blob/master/ur_robot_driver/doc/install_urcap_e_series.md).

To setup the tool communication on an e-Series robot, please consider the [tool communication setup guide](https://github.com/UniversalRobots/Universal_Robots_ROS_Driver/blob/master/ur_robot_driver/doc/setup_tool_communication.md).


### Robot teach pendant

For the **LH7+** system it is required to set up static IP's for each UR arm.

**UR5 Polyscope**: Navigate to `setup robot -> Network`. Setup a static ip for each arm.

For the **LH7+** the ip's is setup as:

  - Right arm: **address:** `192.168.1.137` - **netmask:** `255.255.255.0`
  - Left arm: **address:** `192.168.1.136` - **netmask:** `255.255.255.0`

For a **LH7+** system its **important** to change the **custom ports** and **IP address for the Remote PC** in the **externalcontrol** for each UR5 arm to be different.

**UR5 Polyscope**: Navigate to `Program robot -> installation tab -> externalcontrol tab -> custom_port/ip address`.

Here you can change the custom port for each robot to connect to the ROS pc, aswell as the remote pc's ip address.

For the **LH7+** the ip's is setup as:

  - Custom_port Right arm: `50002` - Remote PC ip: `192.168.1.135`
  - Custom_port Left arm: `50004` - Remote PC ip: `192.168.1.135`

ROS PC static ip setup: `192.168.1.135`

### System launch file

#### Robot launch

The launch file for the robots uses the `ur_common.launch`, file from the **ur_robot_driver** package.
When using this launch file, the following arguments must be specified when launching the instance.

```
<arg name="debug" default="false"/>
<arg name="use_tool_communication" value="false"/>
<arg name="controller_config_file" default="controller_config_file.yaml)"/>
<arg name="robot_ip" value="robot_ip"/>
<arg name="reverse_port" default="50001"/>
<arg name="script_sender_port" default="50002"/>
<arg name="kinematics_config" default="kinematics_config.yaml"/>
<arg name="limited" default="false"/>
<arg name="tf_prefix" default=""/>
<arg name="controllers" default="joint_state_controller"/>
<arg name="stopped_controllers" default="pos_traj_controller"/>
<arg name="headless_mode" default="false"/>
```

Of these arguments, **robot_ip**, **reverse_port**, **script_sender_port**, **tf_prefix**, **controllers** and **controller_config_file**, has to different for each robot connection.

The robot **robot_ip** is setup as:

- Right arm: **address:** `192.168.1.137`
- Left arm: **address:** `192.168.1.136`

The **reverse_port** is setup as:

- Right arm: `50001`
- Left arm: `50003`

The **script_sender_port** is setup as:

- Right arm: `50002`
- Left arm: `50004`

The **tf_prefix** is setup as:

- Right arm: `ur5_right_`
- Left arm: `ur5_left_`

The `controller_config_file`, is a modified version of the `ur_controllers.yaml` found in the **ur_robot_driver** package. It is modified to contain the `tf_prefix` of the respective arm.

The `controllers` argument contains what controllers should be launched for the robot. In this case these are:

- Right arm: `right_joint_state_controller right_scaled_pos_traj_controller`
- Left arm: `left_joint_state_controller left_scaled_pos_traj_controller`


To launch more than one robot, however, each robot should be launched inside a namespace. This is because two ROS nodes with the same name cannot run at the same time.

These namespaces are setup as:

- Right arm: `right_arm`
- Left arm: `left_arm`

Note: Make sure the externalcontrol is running on both robots in **PolyScope**

#### Robotiq 3-Finger Gripper Launch

First a static ip address should be setup on the Robotiq 3f gripper, using the windows application. In this case the address is setup as: `192.168.1.140`

The connection to the Robotiq 3f gripper is launched using the `Robotiq3FGripperTcpNode.py` found in the **robotiq** package. This needs the ip address as an argument. This address the setup as: `192.168.1.140`

#### Intel RealSense Cameras Launch

In order for the two Intel Realsense cameras on the **LH7+** to work the Intel Realsense SDK has to be installed. Go to Intel RealSense's website and [install the Intel RealSense SDK for Linux](https://www.intelrealsense.com/developers/).

The two Intel Realsense d435 cameras are launched using the `rs_camera.launch` file found in the **realsense2_camera** package. For this launch file the following arguments must be setup for each camera:

```
<arg name="depth_width" default="1280"/>
<arg name="depth_height" default="720"/>
<arg name="enable_depth" default="true"/>

<arg name="infra_width" default="1280"/>
<arg name="infra_height" default="720"/>
<arg name="enable_infra1" default="true"/>
<arg name="enable_infra2" default="true"/>

<arg name="color_width" default="1280"/>
<arg name="color_height" default="720"/>
<arg name="enable_color" default="true"/>

<arg name="enable_pointcloud" default="true"/>
<arg name="enable_sync" default="false"/>
<arg name="align_depth" default="true"/>

<arg name="initial_reset" default="true"/>
```

Additionally, each camera needs a tf_prefix and a serial number. These are setup as:

```
<arg name="camera" default="base_camera"/>
<arg name="serial_no" default="934222070800/>
```

```
<arg name="camera" default="wrist_camera"/>
<arg name="serial_no" default="827112072702/>
```

## Pan-Tilt-Unit

Web interface can be accessed at http://192.168.1.110/control.html
