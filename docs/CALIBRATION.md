## Hand eye calibration

This following text is a small tutorial on how to start the hand-eye calibration for the **LH7+**.

### Create AR Tag

Once the repository is successfully compiled, an AR tag should be generated with the following line:

```shell
rosrun ar_track_alvar createMarker -ucm -p
```

After the AR_tag has been created. Print the AR tag to a piece of paper. Once this is done the **ar_alvar_track** package should be configured to the image topic of the camera (In the file called `dummy/launch/ar_alvar.launch`). In the case of the **LH7+** its either `base_cam/color/image_raw` or `wrist_cam/color/image_raw` such as:

```
<arg name="cam_image_topic" default="/wrist_camera/color/image_raw" />
```

Remember to measure the size of the AR tag on the paper and insert the size of the tag in the parameter:

```
<arg name="marker_size" default="8.6" />
```

By default the **ar_track_alvar** package is looking for a AR_tag with the size of **8.6 cm**.

### Configure Easy Handeye

Once this is done the **easy_handeye** package should be configured to the camera and the AR tag. The file can be found at the following location: `dummy/launch/calibrate_robots.launch`. Here the calibration should be configured to what frames of the robot and camera that should be calibrated and what AR tag to use.

Change the `namespace_prefix` to the prefix you what for the calibration.

```
<arg name="namespace_prefix" default="dummy_calibration"
```

In the case of the **LH7+** the `base_frame` should either be:

```
<arg name="robot_base_frame" value= "ur5_right_base_link" or "ur5_left_base_link"
```

Do the same for the `robot_effector_frame`:

```
<arg name="robot_effector_frame" value= "ur5_right_ee_link" or "ur5_left_ee_link"
```

Now the `tracking_base_frame` have to be configured to either of the cameras frames:

```
<arg name="tracking_base_frame" value= "wrist_camera_link" or "base_camera_link"  
```

Change the marker ID to the marker your printed on paper:

```
<arg name="tracking_marker_frame" value= "MARKER_ID"
```

Change the `eye_on_hand` to be either be `True` or `False` depending on whether you want to do a eye on hand or eye on base calibration.

### Run calibration

<!--- Add rviz config to calibration -->

Once **ar_track_alvar** and **easy_handeye** have been configured, the calibration can begin.

The following command is going to launch all the necessary connections to do the calibration, as well as **RViz** for visualising the marker positions in the robot world:

```shell
roslaunch dummy main.launch calibration:=true
```

At this point the calibration can start. Make should that the AR_tag is being detected in **RViz** when taking samples for the calibration.

once the calibration is done and saved the result can be implemented into the launch file for the robots (found in `dummy/launch/realRobotBringup.launch`). This is done in the following manner:

```
<include file="$(find easy_handeye)/launch/publish.launch">
  <arg name="eye_on_hand" value="false" />
  <arg name="namespace_prefix" value="NAME_SPACE"/>
</include>
```

Change the `<arg name="eye_on_hand" value="false" />` to either `True` or `False` depended on whether you performed a **"Eye on Base"** or **"Eye in hand"**

Change the `NAME_SPACE` to what name you added in the file `dummy/launch/calibrate_robots.launch`. By deafult its `dummy_calibration`
