#! /usr/bin/env python
import rospy
import numpy as np
from human_pose_ros.msg import Skeleton, PoseEstimation
from tracking.kalman_utils import kalman_sensor_dict
from tracking.kalman_3d import KalmanBrownian, KalmanConstVelocity, KalmanCustom
from rospy_message_converter import message_converter
from sensor_msgs.msg import CameraInfo
from vision_utils.logger import get_logger, get_printer
from pose_utils.utils import *
from tracking.msg import LaserCentroids
import tf

from dynamic_reconfigure.server import Server
from tracking.cfg import trackingConfig

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--debug',
                 action='store_true',
                 help='Print debug')

args, unknown = parser.parse_known_args()

logger = get_logger()
pprinter = get_printer(depth=3)
CAM_FRAME = "/wrist_camera_depth_optical_frame"
Skeleton_joints = message_converter.convert_ros_message_to_dictionary(Skeleton()).keys()

Skeleton_joints.remove("dummy")
Skeleton_joints.remove("id")

DEPTH_INFO_TOPIC = '/{}_camera/camera/aligned_depth_to_color/camera_info'.format("wrist")

rospy.init_node('skeleton_kalman')

tf_listener = tf.TransformListener()

logger.info("Waiting for camera info :)")
cameraInfo = rospy.wait_for_message(DEPTH_INFO_TOPIC, CameraInfo)
logger.info("Got camera info")


skeletons_cen = []
centroids = []
matched_centroids = []
matched_laser = set()
matched_skel = set()
velocity = 0

fallback_joints = {
    'left_eye': ['left_ear','nose','left_shoulder'],
    'right_eye': ['right_ear','nose','right_shoulder'],
    'left_ear': ['left_eye','nose','left_shoulder'],
    'right_ear': ['right_eye','nose','right_shoulder'],
    'right_ankle': ['right_knee', 'right_hip'],
    'left_ankle': ['left_knee', 'left_hip'],
    'right_knee': ['right_ankle', 'right_hip'],
    'left_knee': ['left_ankle', 'left_hip'],
    'left_wrist': ['left_elbow','left_shoulder'],
    'right_wrist': ['right_elbow','right_shoulder'],
    'left_elbow': ['left_wrist','left_shoulder'],
    'right_elbow': ['right_wrist','right_shoulder'],
    'left_shoulder': ['left_ear','left_elbow', 'left_eye'],
    'right_shoulder': ['right_ear','right_elbow', 'right_eye'],
    'nose': ['left_eye', 'right_eye', 'left_ear', 'right_ear']
}

tracked_skel_missing_joints = set()

def laser_callback(msg):
    global centroids, kalman_dict, matched_centroids
    centroids_copy = []

    tracked_laser_id = matched_centroids[0]['laser_id'] if len(matched_centroids) else None

    if len(msg.centroids):
        for centroid in msg.centroids:
            time = rospy.Time.now()
            centroid.header.stamp = time
            tf_listener.waitForTransform('/world', CAM_FRAME, time, rospy.Duration(0.5))
            transformed_pose = tf_listener.transformPose("wrist_camera_depth_optical_frame",centroid)
            centroids_copy.append([transformed_pose.pose.position.x, transformed_pose.pose.position.y, transformed_pose.pose.position.z])

    centroids = centroids_copy

    if tracked_laser_id is not None and tracked_laser_id <= len(msg.centroids)-1:
        tracked_centroid = centroids_copy[tracked_laser_id]
        # print(tracked_centroid)
        kalman_dict["centroid"].update(tracked_centroid, sensor='skeleton_points', R=kalman_sensor_dict["laser"])
        publish_skel()


    # print("Centroids callback", len(centroids))


def pose_callback(msg):
    # print(msg)
    global skeletons_cen, matched_centroids, kalman_dict, tracked_skel_missing_joints
    skeletons_cen = []

    if len(matched_centroids):
        tracked_skeleton_id = matched_centroids[0]['skel_id']
    else: tracked_skeleton_id = None

    for id,skel_msg in enumerate(msg.skeletons):
        valid_points = []
        valid_joints = set()
        missing_joints = set()
        skeleton_dict = message_converter.convert_ros_message_to_dictionary(skel_msg)
        for joint,coord in skeleton_dict.items():
            if isinstance(coord,list) and len(coord) and kalman_dict.get(joint,0):
                # coord = pixel_to_camera(cameraInfo, (coord[0],coord[1]), coord[2])
                valid_points.append(coord)
                valid_joints.add(joint)
                skeleton_dict[joint] = coord
            elif isinstance(coord,list) and not len(coord) and kalman_dict.get(joint,0):
                missing_joints.add(joint)
                pass

        skeleton_dict["centroid"] = list(get_points_centroid(valid_points))
        skeletons_cen.append(skeleton_dict["centroid"])
        valid_joints.add("centroid")

        if id == tracked_skeleton_id:
            for joint,coord in skeleton_dict.items():
                # print(joint, "coord",coord)
                if joint in valid_joints:
                    if joint in tracked_skel_missing_joints: tracked_skel_missing_joints.remove(joint)
                    joint_name = joint.split("_")[-1] if len(joint.split("_")) else joint
                    measurement = list(coord)
                    kalman_dict[joint].update(measurement, sensor='skeleton_points', R=kalman_sensor_dict[joint_name])
                elif joint in missing_joints:
                    tracked_skel_missing_joints.add(joint)
                    joint_name = joint.split("_")[-1] if len(joint.split("_")) else joint
                    measurement = list(coord)
                    # kalman_dict[joint].update(measurement, sensor='skeleton_points', R=kalman_sensor_dict[joint_name])
            publish_skel()


def find_closest_match():
    global skeletons_cen, centroids, matched_centroids, matched_laser, matched_skel
    combination_list = []
    matched_centroids_copy = []
    save_skel_cen = skeletons_cen
    save_cen = centroids

    if args.debug: logger.info("-- matching --")

    if len(save_skel_cen) or len(save_cen):
        if len(save_skel_cen) < len(save_cen):
            for i in range(len(save_skel_cen),len(save_cen)):
                save_skel_cen.append([])
        else:
            for i in range(len(save_cen),len(save_skel_cen)):
                save_cen.append([])
        for skel_index, skel_centroid in enumerate(save_skel_cen):
            for cen_index, laser_centroid in enumerate(save_cen):
                laser_robot_distance = better_distance_between_points([0]*3, laser_centroid[0:3]) if len(laser_centroid) else None
                skel_robot_distance = better_distance_between_points([0]*3, skel_centroid[0:3]) if len(skel_centroid) else None
                # print(skel_robot_distance, laser_robot_distance)

                distances = []
                for candidate in [laser_robot_distance, skel_robot_distance]:
                    if candidate is not None: distances.append(candidate)

                pair = {
                    'skel_id': skel_index if len(skel_centroid) else -1,
                    'skel_centroid': skel_centroid,
                    'laser_id': cen_index if len(laser_centroid) else -1,
                    'laser_centroid': laser_centroid[0:3],
                    'skel_laser_distance': better_distance_between_points(laser_centroid[0:3], skel_centroid[0:3]) if len(laser_centroid) and len(skel_centroid) else 0,
                    'person_robot_distance': min(distances) if len(distances) else float("inf")
                }
                combination_list.append(pair)


        if args.debug: logger.info("-- Possible pairs --")
        if args.debug: pprinter.pprint(combination_list)

        combination_list = sorted(combination_list, key=lambda k: k['skel_laser_distance'])
        combination_list = sorted(combination_list, key=lambda k: k['person_robot_distance'])

        match = combination_list[0]

        # if args.debug: pprinter.pprint(match)
        # print(distances_to_world)

        matched_centroids_copy.append(match)
            # print("len of matched centroids",len(matched_centroids))
        save_skel_cen = []
        save_cen = []

        if len(matched_centroids_copy):
            matched_centroids = matched_centroids_copy
        else:
            matched_centroids = []

        if args.debug: logger.info("-- Sorted matches --")
        if args.debug: pprinter.pprint(matched_centroids)


def vel_cb(msg):
    skel_dict = message_converter.convert_ros_message_to_dictionary(msg)
    velocities = [v[0] for v in skel_dict.values() if isinstance(v,list) and len(v)]

    velocity = np.mean(velocities)
    measurement = [velocity]*6
    # if velocity>0:
    #     for joint in Skeleton_joints:
    #         kalman_dict[joint].update(measurement, R=kalman_sensor_dict['velocity'], sensor='skeleton_velocity')

pose_sub = rospy.Subscriber('openpifpaf_pose_transformed_pose_cam', PoseEstimation, pose_callback)
filter_pub = rospy.Publisher('openpifpaf_pose_kalman', PoseEstimation, queue_size=10)
centroid_sub = rospy.Subscriber('scan_centroids', LaserCentroids, laser_callback)

skel_velocity_sub = rospy.Subscriber('skel_velocity_10', Skeleton, vel_cb)
kalman_dict = {}

def publish_skel():
    pose_msg = PoseEstimation()
    pose_msg.tracked_person_id = 0
    skel = dict()

    for joint in Skeleton_joints:
        coords = kalman_dict[joint].get_state()
        # if args.debug: logger.debug("Kalman state: {} {}".format(joint, coords))
        uncertainty = kalman_dict[joint].get_uncertainty()
    #     # print(uncertainty)
    #     if joint in tracked_skel_missing_joints:
    #         measurement = kalman_dict["centroid"].get_state()[0:3]
    #         if joint in fallback_joints:
    #             ref_joints = [jnt for jnt in fallback_joints[joint] if jnt not in tracked_skel_missing_joints]
    #             if len(ref_joints):
    #                 measurement = kalman_dict[ref_joints[0]].get_state()[0:3]
    #                 logger.info("Using ref {} for missing joint {}".format(ref_joints[0], joint))
    #         coords = measurement
    #
    #
        skel[joint] = list(coords[0:3])
        skel[joint].extend(kalman_dict[joint].get_uncertainty_pos())

    skeleton_msg = message_converter.convert_dictionary_to_ros_message("human_pose_ros/Skeleton",skel)
    # print(skeleton_msg)

    pose_msg.skeletons.append(skeleton_msg)

    filter_pub.publish(pose_msg)

r = rospy.Rate(20)
kalman_rate = 0.05

def reinitialize_filters(q):
    global kalman_dict
    kalman_dict_copy = kalman_dict
    for joint in Skeleton_joints:
        kalman_dict_copy[joint] = KalmanBrownian(T=kalman_rate, q=q)
    kalman_dict = kalman_dict_copy

def cfg_callback(config, level):
    rospy.loginfo("""Reconfigure Request: {kalman_process_noise}""".format(**config))

    reinitialize_filters(q=config['kalman_process_noise'])

    return config

srv = Server(trackingConfig, cfg_callback)


while not rospy.is_shutdown():

    if len(skeletons_cen) or len(centroids):
        find_closest_match()
    for joint in Skeleton_joints:
        kalman_dict[joint] = kalman_dict.get(joint, KalmanBrownian(T=kalman_rate))

        kalman_dict[joint].predict()

    r.sleep()
