#! /usr/bin/env python

import numpy as np
from filterpy.kalman import KalmanFilter
from filterpy.common import Q_discrete_white_noise

sensors = {
    'skeleton_points': np.array([[1., 0., 0.],
                                 [0., 1., 0.],
                                 [0., 0., 1.]])
}

class KalmanConstVelocity:
    def __init__(self, T=0.1, q = 0.1):
        self.f = KalmanFilter (dim_x=6, dim_z=3)

        # Initial state
        self.f.x = np.array([[0.],    # position
                             [0.],    # position
                             [0.],    # position
                             [0.],    # velocity
                             [0.],    # velocity
                             [0.]])   # velocity


        # State transistion matrix (Constant velocity model)
        self.f.F = np.array([[1., 0., 0., T, 0., 0.],
                             [0., 1., 0., 0., T, 0.],
                             [0., 0., 1., 0., 0., T],
                             [0., 0., 0., 1., 0., 0.],
                             [0., 0., 0., 0., 1., 0.],
                             [0., 0., 0., 0., 0., 1.]])

        # Sensor model
        self.f.H = np.array([[1., 0., 0., 0., 0., 0.],
                             [0., 1., 0., 0., 0., 0.],
                             [0., 0., 1., 0., 0., 0.]])

        # Initial covariance (Identity * 10)
        self.f.P *= 1.

        # Sensor noise
        self.f.R = np.array([[0.1, 0., 0.],
                             [0., 0.1, 0.],
                             [0., 0., 0.1]])

        self.f.id = np.array([[1., 0., 0., 0., 0., 0.],
                             [0., 1., 0., 0., 0., 0.],
                             [0., 0., 1., 0., 0., 0.],
                             [0., 0., 0., 1., 0., 0.],
                             [0., 0., 0., 0., 1., 0.],
                             [0., 0., 0., 0., 0., 1.]])
        # Process noise
        self.f.Q = np.array([[0.0009, 0., 0., 0., 0., 0.],
                             [0., 0.0009, 0., 0., 0., 0.],
                             [0., 0., 0.0009, 0., 0., 0.],
                             [0., 0., 0., 0.0009, 0., 0.],
                             [0., 0., 0., 0., 0.0009, 0.],
                             [0., 0., 0., 0., 0., 0.0009]])


    def predict(self):
        self.f.predict()
        return self.f.x

    def update(self, z, R=None, sensor='skeleton_points'):

        self.f.update(z, H=self.f.H, R=R)
        return self.f.x

    def get_state(self):
        return self.f.x

    def get_uncertainty(self):
        return self.f.P

    def get_uncertainty_pos(self):
        return (np.sqrt(self.f.P[0][0]), np.sqrt(self.f.P[1][1]), np.sqrt(self.f.P[2][2]))


class KalmanBrownian(KalmanConstVelocity):
    def __init__(self, T=0.1, q = 0.1):
            self.f = KalmanFilter (dim_x=3, dim_z=3)

            # Initial state
            self.f.x = np.array([[0.],    # position
                                 [0.],    # position
                                 [0.]])     # position


            # State transistion matrix (Constant velocity model)
            self.f.F = np.array([[1., 0., 0.],
                                 [0., 1., 0.],
                                 [0., 0., 1.]])

            # Sensor model
            self.f.H = np.array([[1., 0., 0.],
                                 [0., 1., 0.],
                                 [0., 0., 1.]])

            # Initial covariance (Identity * 10)
            self.f.P *= 1.

            # Sensor noise
            self.f.R = np.array([[1., 0., 0.],
                                 [0., 1., 0.],
                                 [0., 0., 1.]])

            # Process noise
            self.f.Q = np.array([[0.0009, 0, 0],
                            [0, 0.0009, 0],
                            [0, 0, 0.0009]])

class KalmanCustom(KalmanConstVelocity):
    def __init__(self, T=0.1, q = 0.1):
        self.f = KalmanFilter (dim_x=3, dim_z=3)

        # Initial state
        self.f.x = np.array([[0.],    # position
                             [0.],    # position
                             [0.]])     # position


        # State transistion matrix (Constant velocity model)
        self.f.F = np.array([[1., 0., 0.],
                             [0., 1., 0.],
                             [0., 0., 1.]])

        # Sensor model
        self.f.H = np.array([[1., 0., 0.],
                             [0., 1., 0.],
                             [0., 0., 1.]])

        # Initial covariance (Identity * 10)
        self.f.P *= 1.

        # Sensor noise
        self.f.R = np.array([[1., 0., 0.],
                             [0., 1., 0.],
                             [0., 0., 1.]])

        # Process noise
        self.f.Q = np.array([[0.0009, 0, 0],
                        [0, 0.0009, 0],
                        [0, 0, 0.0009]])
