#! /usr/bin/env python

import numpy as np
import rospy
import math
import cv2
import tf
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import PoseStamped
from skimage.measure import label
from skimage.morphology import square, dilation

from visualization_msgs.msg import Marker
from tracking.msg import LaserCentroids

prev_img = None


def laser_cb(laserScan):
    global prev_img

    # Convert to world
    curr_img  = np.zeros([800,1000])

    world_x = np.zeros([len(laserScan.ranges)])
    world_y = np.zeros([len(laserScan.ranges)])
    for i in range(0, len(laserScan.ranges)):
        world_x[i] = laserScan.ranges[i]*math.cos(laserScan.angle_min+laserScan.angle_increment*i)
        world_y[i] = laserScan.ranges[i]*math.sin(laserScan.angle_min+laserScan.angle_increment*i)

    # Remove useless data
    world_x = world_x[~np.isnan(world_x)]
    world_x = world_x[~np.isneginf(world_x)]
    world_x = world_x[~np.isinf(world_x)]
    world_y = world_y[~np.isnan(world_y)]
    world_y = world_y[~np.isneginf(world_y)]
    world_y = world_y[~np.isinf(world_y)]

    world_x += 2
    world_y += 5

    centroids = []
    transformed_centroids = []

    for i in range(len(world_x)):
        curr_img[int(world_x[i]*100)][int(world_y[i]*100)] = 1

    # cv2.imshow("Raw image", curr_img)
    if prev_img is not None:

        new_img = prev_img + curr_img
        new_img[new_img > 1] = 1


        mask = np.zeros_like(new_img)
        mask[170:410, 390:590] = 1


        mask_marker = Marker()
        mask_marker.header.frame_id = "/laser"
        mask_marker.type = mask_marker.SPHERE
        mask_marker.action = mask_marker.ADD

        mask_marker.id = 200
        mask_marker.color.a = 1.0
        mask_marker.scale.x, mask_marker.scale.y, mask_marker.scale.z = 0.1, 0.1, 0.1
        mask_marker.color.r, mask_marker.color.g, mask_marker.color.b = (0.0,0.0,1.0)
        mask_marker.pose.orientation.w = 1.0
        mask_marker.lifetime = rospy.Duration(0.2)
        mask_marker.header.stamp = rospy.get_rostime()

        mask_marker.pose.position.x = float(170 / 100.0) - 2.0
        mask_marker.pose.position.y = float(390 / 100.0) - 5.0
        mask_marker.pose.position.z = 0
        # center_pub.publish(mask_marker)
        mask_marker.id = 300
        mask_marker.pose.position.x = float(410 / 100.0) - 2.0
        mask_marker.pose.position.y = float(590 / 100.0) - 5.0
        # center_pub.publish(mask_marker)

        # cv2.imshow("Mask", mask)
        new_img[mask < 1] = 0

        # cv2.imshow("Masked image", new_img)

        new_img = dilation(new_img, square(51))

        # cv2.imshow("Dilated image", new_img)

        labels = label(new_img, connectivity=2)


        for i in range(np.max(labels)+1):
            if i is not 0:
                blob = np.zeros_like(labels)
                blob[labels == i] = 1

                x_center, y_center = np.argwhere(blob == 1).mean(0)

                centroid = Marker()
                centroid.header.frame_id = "/laser"
                centroid.type = centroid.SPHERE
                centroid.action = centroid.ADD

                centroid.id = i
                centroid.color.a = 1.0
                centroid.scale.x, centroid.scale.y, centroid.scale.z = 0.1, 0.1, 0.1
                centroid.color.r, centroid.color.g, centroid.color.b = (1.0,1.0,1.0)
                centroid.pose.orientation.w = 1.0
                centroid.lifetime = rospy.Duration(0.2)
                centroid.header.stamp = rospy.Time.now()

                centroid.pose.position.x = float(x_center / 100.0) - 2.0
                centroid.pose.position.y = float(y_center / 100.0) - 5.0
                centroid.pose.position.z = 0

                pose = PoseStamped()
                pose.header.stamp = rospy.Time.now()
                pose.header.frame_id = "/laser"
                pose.pose = centroid.pose

                centroids.append(pose)
                center_pub.publish(centroid)


        listener.waitForTransform("/world", "/laser", rospy.Time.now(),rospy.Duration(1.0))

        for pose in centroids:
            transformed_centroids.append(listener.transformPose("world",pose))
        centroids_pub.publish(transformed_centroids)

    # cv2.waitKey(1)

    prev_img = curr_img



rospy.init_node("laser_tracker")
listener = tf.TransformListener()
rospy.Subscriber("scan", LaserScan, laser_cb)
center_pub = rospy.Publisher('center_hokuyo', Marker, queue_size=100)
centroids_pub = rospy.Publisher('scan_centroids', LaserCentroids, queue_size=1)

print("laser_tracker node running")
rospy.spin()
