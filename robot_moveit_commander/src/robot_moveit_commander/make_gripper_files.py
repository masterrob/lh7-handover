import rospy
import pypcd
import sys
from cv_bridge import CvBridge
import cv2
from sensor_msgs.msg import PointCloud2, Image


def main():
    rospy.init_node('make_a_fucking_pcd_file_for_Rune')

    msg = rospy.wait_for_message('/camera/depth_registered/points', PointCloud2)
    rosImg = rospy.wait_for_message('/camera/color/image_raw', Image)

    bridge = CvBridge()
    img = bridge.imgmsg_to_cv2(rosImg)


    pc = pypcd.pypcd.PointCloud.from_msg(msg)
    pathPcd = sys.path[0] + '/../pointCloud.txt'
    pathImg = sys.path[0] + '/../BG.png'
    # path = '%s/../resources/pointCloud%s.pcd'
    # pypcd.save_point_cloud(pc, pathPcd)

    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    cv2.imwrite(pathImg, img)
