import roslib
roslib.load_manifest('robotiq_3f_gripper_control')
import rospy
from robotiq_3f_gripper_articulated_msgs.msg import *
from std_msgs.msg import Float32MultiArray

class FakeGripper:

    command = Float32MultiArray()
    finger_poses = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    gripper_pub = rospy.Publisher('finger_pose', Float32MultiArray, queue_size=20)

    def genCommand(self, request):
        self.command = Float32MultiArray()

        # Generate command to close the gripper
        if request == 'close':
            self.finger_poses[0] = 0.8
            self.finger_poses[1] = 0.8
            self.finger_poses[2] = 0.8

        # Generate command to open the gripper
        if request == 'open':
            self.finger_poses[0] = 0.0
            self.finger_poses[1] = 0.0
            self.finger_poses[2] = 0.0

        # Generate command to set the gripper to "basic" mode
        if request == 'basic':
            self.finger_poses[3] = 0.0
            self.finger_poses[4] = 0.0

        # Generate command to set the gripper to "pinch" mode
        if request == 'pinch':
            self.finger_poses[3] = -0.2
            self.finger_poses[4] = 0.2

        # Generate command to set the gripper to "wide" mode
        if request == 'wide':
            self.finger_poses[3] = 0.2
            self.finger_poses[4] = -0.2

        if request == 'left_open':
            self.finger_poses[5] = 0.05
            self.finger_poses[6] = 0.05

        if request == 'left_close':
            self.finger_poses[5] = 0.0
            self.finger_poses[6] = 0.0

        if request == 'right_open':
            self.finger_poses[7] = 0.05
            self.finger_poses[8] = 0.05

        if request == 'right_close':
            self.finger_poses[7] = 0.0
            self.finger_poses[8] = 0.0

        self.command.data = self.finger_poses

        self.gripper_pub.publish(self.command)



class RSgripper:

    # Initiating the gripper_pub to be set in the init function
    # Creating the gripper command as a global variable within the class
    gripper_pub = rospy.Publisher(
            'Robotiq3FGripperRobotOutput', Robotiq3FGripperRobotOutput, queue_size=20)
    command = Robotiq3FGripperRobotOutput()

    # Init function for the gripper class
    # Initiates the gripper_pub as the publisher being parsed from the robot_class
    def startupGripper(self):
        self.resetGripper()
        self.activateGripper()

    # Function for resetting the gripper
    def resetGripper(self):
        # Creates the reset command using the genCommand Function
        self.genCommand("reset")
        # Publishes the gripper command to the gripper connection node
        self.gripper_pub.publish(self.command)

        # While loop to keep the program from continuing, unless the gripper is done resetting
        while True:
            msg = rospy.wait_for_message('Robotiq3FGripperRobotInput', Robotiq3FGripperRobotInput)
            if msg.gACT == 0:
                print 'Reset complete'
                break

    # Function for activating the gripper
    def activateGripper(self):
        # Gets feedback from the gripper to check if the gripper is already activated
        msg = rospy.wait_for_message('Robotiq3FGripperRobotInput', Robotiq3FGripperRobotInput)
        # if msg.gACT == 1:
        #     # if the gripper is already avtivated skip to next function
        #     print 'Gripper already activated'
        # else:
        print 'Activating gripper wait 5 secounds'
        # If the gripper is not activated generate the activate command, and publish the command
        self.genCommand("activate")
        # print 'Activating gripper'
        self.gripper_pub.publish(self.command)

        # Lock the program from continuing untill the gripper is done activating
        while True:
            msg = rospy.wait_for_message('Robotiq3FGripperRobotInput', Robotiq3FGripperRobotInput)
            if msg.gIMC == 3:
                print 'Activation complete'
                break
                    

    # Function for opening the gripper
    def openGripper(self):
        # Generate command for opening the gripper
        self.genCommand("open")
        print 'Opening gripper'
        self.gripper_pub.publish(self.command)
        # Publish the message to the gripper ndoe
        # self.publishMessage()

    # Function for publishing commands to the gripper node
    def publishMessage(self):

        # Publish the command
        self.gripper_pub.publish(self.command)
        #
        # # Get feedback from the gripper
        # msg = rospy.wait_for_message('Robotiq3FGripperRobotInput', Robotiq3FGripperRobotInput)
        # # Waiting for the gripper to complete the movement
        # while msg.gSTA == 0:
        #     msg = rospy.wait_for_message('Robotiq3FGripperRobotInput', Robotiq3FGripperRobotInput)
        #
        # # Check status after gripper movement
        # if msg.gSTA == 1:
        #     print '1 or 2 fingers blocked'
        #     return True
        # if msg.gSTA == 2:
        #     print 'All fingers stopped'
        #     return True
        # if msg.gSTA == 3:
        #     print 'Movement complete'
        #
        # # CHeck each finger to see if any was stopped during the movement
        # if msg.gDTA == 1 or msg.gDTA == 2:
        #     print 'Finger A stopped'
        # if msg.gDTB == 1 or msg.gDTB == 2:
        #     print 'Finger B stopped'
        # if msg.gDTC == 1 or msg.gDTC == 2:
        #     print 'Finger C stopped'


    # Function for controlling each finger independenly
    def individualFingerControl(self, pos1, pos2, pos3):

        # Setting rICF to 1, to enable individual finger control
        self.command.rICF = 1

        # Setting the pose for each of the fingers
        self.command.rPRA = int(pos1)
        self.command.rPRB = int(pos2)
        self.command.rPRC = int(pos3)

        # Setting the speed for all fingers to max
        self.command.rSPA = 255
        self.command.rSPB = 255
        self.command.rSPC = 255

        # Publishing the command using the publish function
        print 'Moving fingers independenly'
        # self.publishMessage()

    # Function for setting the gripper back to normal control
    def collectiveFingerControl(self):

        # Setting rICF to 0, to disable individual finger control
        self.command.rICF = 0

        # Publishing the command using the publish function
        print 'Moving fingers collectively'
        # self.publishMessage()


    # Function for generating commands for the gripper
    def genCommand(self, request):
        """Update the command according to the request entered by the user."""

        # Generate command to activate the gripper
        if request == 'activate':
            # Resetting the command msg, as well as setting the rACT and rGTO to 1, to activate the gripper
            self.command = Robotiq3FGripperRobotOutput()
            self.command.rACT = 1
            self.command.rGTO = 1
            self.command.rSPA = 255
            self.command.rFRA = 150

        # Generate command to reset the gripper
        if request == 'reset':
            # Resetting the command msg, as well as setting the rACT to 0 to reset the gripper
            self.command = Robotiq3FGripperRobotOutput()
            self.command.rACT = 0

        # Generate command to close the gripper
        if request == 'close':
            self.command.rPRA = 255

        # Generate command to open the gripper
        if request == 'open':
            self.command.rPRA = 0

        # Generate command to set the gripper to "basic" mode
        if request == 'basic':
            self.command.rMOD = 0

        # Generate command to set the gripper to "pinch" mode
        if request == 'pinch':
            self.command.rMOD = 1

        # Generate command to set the gripper to "wide" mode
        if request == 'wide':
            self.command.rMOD = 2

        # Generate command to set the gripper to "scissor" mode
        if request == 'scissor':
            self.command.rMOD = 3

        # If the self.command entered is a int, assign this value to rPRA
        try:
            self.command.rPRA = int(request)
            if self.command.rPRA > 255:
                self.command.rPRA = 255
            if self.command.rPRA < 0:
                self.command.rPRA = 0
        except ValueError:
            pass

        # Generate command to make the gripper move faster
        if request == 'faster':
            self.command.rSPA += 25
            if self.command.rSPA > 255:
                self.command.rSPA = 255

        # Generate command to make the gripper move slower
        if request == 'slower':
            self.command.rSPA -= 25
            if self.command.rSPA < 0:
                self.command.rSPA = 0

        # Generate command to make the gripper use more force
        if request == 'forceInc':
            self.command.rFRA += 25
            if self.command.rFRA > 255:
                self.command.rFRA = 255

        # Generate command to make the gripper less force
        if request == 'forceDec':
            self.command.rFRA -= 25
            if self.command.rFRA < 0:
                self.command.rFRA = 0


        return self.publishMessage()
