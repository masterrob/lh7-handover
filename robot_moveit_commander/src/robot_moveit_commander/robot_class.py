import sys
import rospy
import numpy as np
import moveit_commander
import socket
import signal
from contextlib import contextmanager
import moveit_msgs.msg
from sensor_msgs.msg import PointCloud2, PointCloud
import sensor_msgs.point_cloud2 as pc2
from geometry_msgs.msg import Pose
from control_msgs.msg import FollowJointTrajectoryActionResult


# Importing openCV to use the waitkey function to create a trigger for movement
import cv2

import robot_moveit_commander
from robot_moveit_commander import gripper_class as gripper
from robotiq_3f_gripper_articulated_msgs.msg import Robotiq3FGripperRobotOutput


class Robot:
    # Setting up the moveit
    # Creating a RobotCommander to control the arms
    # Creating  a PlanningSceneInterface to monitor the robots current state
    moveit_commander.roscpp_initialize(sys.argv)
    robot = moveit_commander.RobotCommander()
    scene = moveit_commander.PlanningSceneInterface()

    # Setting up the different move groups created in the MoveIt! setup assistant
    left_arm = moveit_commander.MoveGroupCommander("left_arm")
    right_arm = moveit_commander.MoveGroupCommander("right_arm")
    both_arms = moveit_commander.MoveGroupCommander("both_arms")
    # gripper_group = moveit_commander.MoveGroupCommander("gripper")

    # Creating a publisher for the gripper control
    # Creating a publisher for displaying a robot trajectory
    display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path',
                                                   moveit_msgs.msg.DisplayTrajectory,
                                                   queue_size=20)

    left_result = None
    right_result = None
    isPose = False


    left_arm.allow_replanning(True)
    right_arm.allow_replanning(True)
    both_arms.allow_replanning(True)
    # gripper_group.allow_replanning(True)


    # Reducing the planning time for the robot from 5 seconds to 0.5 seconds
    left_arm.set_planning_time(3)
    right_arm.set_planning_time(3)
    both_arms.set_planning_time(3)
    # gripper_group.set_planning_time(3)

    left_arm.set_num_planning_attempts(25)
    right_arm.set_num_planning_attempts(25)
    both_arms.set_num_planning_attempts(25)
    # gripper_group.set_num_planning_attempts(25)


    sim = False

    # Initiating the robotiq-3f gripper, and parsing the publisher for the gripper control
    if sim:
        rs_gripper = gripper.FakeGripper()
    elif not sim:
        rs_gripper = gripper.RSgripper()


    # path = sys.path[0] + '/../../robot_moveit_commander/resources/spacebar.png'

    # img = cv2.imread(path)
    # img = img[300:370, 330:670]
    # Init function being run when the class is being initialized
    # def __init__(self):
    # Resetting gripper in case of it already being activated
    # self.rs_gripper.resetGripper()
    # # Activating the gripper
    # self.rs_gripper.activateGripper()



    def moveRobot(self, msg):
        """Function to move the robot based on placement_destination"""

        arm = self.grasp_arm
        if arm == "left":
            move_group = self.left_arm
        if arm == "right":
            move_group = self.right_arm

        goalPose = Pose()
        poseStamped = move_group.get_current_pose()
        goalPose.orientation = poseStamped.pose.orientation

        if msg.placement_destination == "left":
            goalPose.position.x = -0.2
            goalPose.position.y = 0.2
            goalPose.position.z = 1.2

        if msg.placement_destination == "middle":
            goalPose.position.x = -0.0
            goalPose.position.y = 0.2
            goalPose.position.z = 1.2

        if msg.placement_destination == "right":
            goalPose.position.x = 0.2
            goalPose.position.y = 0.2
            goalPose.position.z = 1.2

        self.setTarget(goalPose, arm)

    def pixelToWorld(self, x, y, camera):
        """Transforms a pixel value into robot world coordinates"""
        point = np.array([x, y, 0])
        topic = '/' + camera + '_camera/depth_registered/points'
        msg = rospy.wait_for_message(
            topic, PointCloud2)
        depth = pc2.read_points(msg, field_names=(
            "x", "y", "z"), skip_nans=True, uvs=[(x, y)])  # Questionable
        cam_point = list(depth)
        print "point found"
        break1 = False
        if(cam_point == []):
            for x_point in range(-5, 5):
                for y_point in range(-5, 5):
                    tempx = x + x_point
                    tempy = y + y_point
                    depth = pc2.read_points(msg, field_names=("x", "y", "z"), skip_nans=True, uvs=[
                                            (tempx, tempy)])  # Questionable
                    cam_point = list(depth)
                    # print cam_point
                    if(cam_point != []):
                        print "point found"
                        break1 = True
                        break
                if break1:
                    break

        tf_listener = tf.TransformListener()
        camera_frame = '/' + camera + '_camera_color_optical_frame'
        tf_listener.waitForTransform(
            '/world', camera_frame, rospy.Time(), rospy.Duration(4))
        (trans, rot) = tf_listener.lookupTransform(
            '/world', camera_frame, rospy.Time())

        world_to_cam = tf.transformations.compose_matrix(
            translate=trans, angles=tf.transformations.euler_from_quaternion(rot))
        obj_vector = np.concatenate((point, np.ones(1))).reshape((4, 1))
        obj_base = np.dot(world_to_cam, obj_vector)
        print("obj_base: ", obj_base[0:3])

        return obj_base[0:3]
        # return cam_point

    def trigger(self):
        """ Trigger to replace rviz_visual_tools trigger"""
        print 'Hit the spacebar'
        cv2.imshow('HIT SPACEBAR', self.img)
        k = []
        while k != 32:
            k = cv2.waitKey(0) & 0xff
            if k == 27:
                cv2.destroyAllWindows()
                print 'Stopping program'
                exit(0)

        cv2.destroyAllWindows()
        print 'Continuing with movement'

    def setNamedTarget(self, target, move_group, move=True):
        """
        Function for setting the robot to a named target
        target: name of target
        move_group: name of MoveGroupCommander ("left", "right" or "both")
        """
        self.isPose = True
        state = self.robot.get_current_state()

        self.left_arm.set_start_state(state)
        self.right_arm.set_start_state(state)
        self.both_arms.set_start_state(state)
        
        if move_group == "left":
            self.left_arm.set_named_target(target)
            plan_success, plan, planning_time, error_code = self.left_arm.plan()

        if move_group == "right":
            self.right_arm.set_named_target(target)
            plan_success, plan, planning_time, error_code = self.right_arm.plan()

        if move_group == "both":
            self.both_arms.set_named_target(target)
            plan_success, plan, planning_time, error_code = self.both_arms.plan()

        if plan_success:  # True if trajectory contains points
            self.displayTrajectory(plan)
            if move: self.executeTrajectory(plan, move_group = move_group)
        else:
            # rospy.logerr("Trajectory is empty. Planning was unsuccessful.", error_code)
            print("Trajectory is empty. Planning was unsuccessful: ", error_code)
            return error_code


    def setTarget(self, target, move_group, move=True, return_plan=True, comment=''):
        """
        Function for setting a target for the robots
        target: pose msg or array of 6 values
        move_group: name of MoveGroupCommander ("left", "right" or "both")
        """
        error = False
        state = self.robot.get_current_state()

        self.both_arms.set_start_state(state)
        self.right_arm.set_start_state(state)
        self.left_arm.set_start_state(state)

        group = None

        if move_group == "left":
            group = self.left_arm  
        elif move_group == "right":
            group = self.right_arm
        elif move_group == "both":
            group = self.both_arms
            group.set_pose_target(
            target[0:5], self.left_arm.get_end_effector_link())
            group.set_pose_target(
                target[6:11], self.right_arm.get_end_effector_link())
        else:
            print("Invaild group")
        
        group.set_pose_target(target)

        plan_success, plan, planning_time, error_code = group.plan()
        if plan_success:  # True if trajectory contains points
            print("successfully planned")
            self.displayTrajectory(plan)
            if move:
                group.execute(plan, wait=True)
                group.clear_pose_targets()
            if return_plan:
                return plan, error_code
        else:
            rospy.logerr("Trajectory is empty. Planning was unsuccessful.")
            if return_plan:
                return plan, error_code
            else:
                return error_code
            

    def linear_move(self, move_group_string, waypoints, comment=''):
        error = False
        self.isPose = False
        state = self.robot.get_current_state()
        
        if move_group_string is "left":
            move_group = self.left_arm
            

        if move_group_string is "right":
            move_group = self.right_arm

        move_group.set_start_state(state)
        fraction = 0.0
        for count_cartesian_path in range(0, 10):
            if fraction <= 1.0:
                (plan_cartesian, fraction) = move_group.compute_cartesian_path(
                    waypoints,   # waypoints to follow
                    0.01,        # eef_step
                    5.0)         # jump_threshold
            else:
                break

        if plan_cartesian.joint_trajectory.points:  # True if trajectory contains points
            print("successfully planned" + comment)
            self.displayTrajectory(plan_cartesian)
            self.executeTrajectory(plan=plan_cartesian, move_group="right")
        else:
            rospy.logerr("Trajectory is empty. Planning was unsuccessful.",comment)
            error = True
            return error
        # self.display_trajectory(plan)



    # Function for planning a trajectory based on a goal target pose
    def planTrajectory(self):
        """ Plan a trajectory """
        # Set the start state of the robot to the current state, to make sure the start state is updated,
        # as it can cause problems if it is not done
        error = False

        # Planning the trajectory of the robot
        plan = self.both_arms.plan()
        if plan.joint_trajectory.points:  # True if trajectory contains points
            print("successfully planned")
            self.displayTrajectory(plan)
        else:
            rospy.logerr("Trajectory is empty. Planning was unsuccessful.")
            error = True

        # Sending the plan to be published
        return plan, error

    # Function for displaying a robot trajectory, using the plan made in the planTrajectory function

    def displayTrajectory(self, plan):
        """ Display the trajectory in the given plan """
        # Create an object containing the trajectory extracted from the plan
        display_trajectory = moveit_msgs.msg.DisplayTrajectory()
        display_trajectory.trajectory_start = self.robot.get_current_state()
        display_trajectory.trajectory.append(plan)
        # publish the trajectory
        self.display_trajectory_publisher.publish(display_trajectory)
        # self.executeTrajectory(plan)

    def executeTrajectory(self, plan, move_group = None, comment=''):
        """ Execute the trajectory previously planned """
        self.left_result = False
        self.right_result = False


        if move_group == "right":
            self.right_arm.execute(plan,wait=True)
        if move_group == "left":
            self.left_arm.execute(plan,wait=True)
        if move_group == "both":
            self.both_arms.execute(plan, wait = True)

        self.both_arms.clear_pose_targets()
        self.right_arm.clear_pose_targets()
        self.left_arm.clear_pose_targets()
        print('goal reached ' + comment)

    def gripperTest(self, command):
        """Function for testing simulated gripper control"""

        self.rs_gripper.genCommand(command)
