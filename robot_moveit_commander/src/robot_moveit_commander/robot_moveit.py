import rospy
from sensor_msgs.msg import PointCloud2
from ccn.msg import RobotMessage
from ccn.srv import Robot
import robot_moveit_commander
from robot_moveit_commander import robot_class as robot



def robot_service(req):
    msg = req.robot_message
    global robot_client

    if msg.action == 'move':
        print('moving robot to: '); print(msg.placement_destination)
        robot_client.moveRobot(msg, arm="temp")
        return 0
        # robot_client.trigger()
        # robot_client.executeTrajectory()
        # Set robot goal to destination
        # Plan trajectory
        # Display trajectory
        # Place cv trigger for safety reasons
        # Move robot to destination

    if msg.action == 'pickup':
        print('Picking up object at: '); print(msg.obj_pose)
        # Set robot goal close to object position
        # Plan trajectory
        # Display trajectory
        # Place cv trigger for safety reasons
        # Move robot to object position
        # Set robot goal to object position
        # Plan trajectory
        # Display trajectory
        # Place cv trigger for safety reasons
        # Move robot to object
        # Grasp the object with the gripper
        # Set robot goal above the table
        # Plan trajectory
        # Display trajectory
        # Place cv trigger for safety reasons
        # Lift the object

    if msg.action == 'dont remember':
        print('something will happen here')

    if msg.action == 'no idea':
        print('something else will happen here')

    robot_client.gripperTest('close')


    # point = robot_client.pixelToWorld(300, 200)
    # print(point)

    # robot_client.trigger()

    return 0


def main():
    global robot_client
    rospy.init_node('robot_commander')
    robot_client = robot.Robot()
    rospy.Service('robot_moveit_service', Robot, robot_service)


    rospy.spin()
    # robot_client.test()
    # robot_client.setTarget(0)
